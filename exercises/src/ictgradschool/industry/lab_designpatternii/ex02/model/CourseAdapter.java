package ictgradschool.industry.lab_designpatternii.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
    /**********************************************************************
     * YOUR CODE HERE
     */
    private Course course;

    private String[] columnNames = {"StudentID", "Surname", "Forename", "Exam", "Test", "Assignment", "Overall"};

    public CourseAdapter(Course course) {
        this.course = course;
        course.addCourseListener(this);
    }

    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return course.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public String getColumnName(int column) {
        if (column==0){
            return columnNames[0];
        }else if (column==1){
            return columnNames[1];
        }else if (column==2){
            return columnNames[2];
        }else if (column==3){
            return columnNames[3];
        }else if (column==4){
            return columnNames[4];
        }else if (column==5){
            return columnNames[5];
        }else if (column==6){
            return columnNames[6];
        }
        return null;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            return course.getResultAt(rowIndex)._studentID;
        } else if (columnIndex == 1) {
            return course.getResultAt(rowIndex)._studentSurname;
        } else if (columnIndex == 2) {
            return course.getResultAt(rowIndex)._studentForename;
        } else if (columnIndex == 3) {
            return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Exam);
        } else if (columnIndex == 4) {
            return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Test);
        } else if (columnIndex == 5) {
            return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Assignment).intValue();
        } else if (columnIndex == 6) {
            return course.getResultAt(rowIndex).getAssessmentElement(StudentResult.AssessmentElement.Overall).intValue();
        }

        return null;
    }


}