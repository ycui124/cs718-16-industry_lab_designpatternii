package ictgradschool.industry.lab_designpatternii.ex02.gui;

import ictgradschool.industry.lab_designpatternii.ex02.model.Course;
import ictgradschool.industry.lab_designpatternii.ex02.model.CourseListener;

import java.awt.*;

public class DistributionPanelAdapter implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */
    private DistributionPanel dp;

    public DistributionPanelAdapter(DistributionPanel dp) {
        this.dp = dp;
    }

    @Override
    public void courseHasChanged(Course course) {
        dp.repaint();
    }


}
