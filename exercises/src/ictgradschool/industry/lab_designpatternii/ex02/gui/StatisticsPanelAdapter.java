package ictgradschool.industry.lab_designpatternii.ex02.gui;

import ictgradschool.industry.lab_designpatternii.ex02.model.Course;
import ictgradschool.industry.lab_designpatternii.ex02.model.CourseListener;

import java.awt.*;

public class StatisticsPanelAdapter implements CourseListener {

    /**********************************************************************
     * YOUR CODE HERE
     */
    private StatisticsPanel sp;

    public StatisticsPanelAdapter(StatisticsPanel sp) {
        this.sp = sp;
    }

    @Override
    public void courseHasChanged(Course course) {
        sp.repaint();
    }


}
